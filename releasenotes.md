## Jetshop Responsive Base Framework
# Releasenotes

##1.5.1 (2018-05-04)

- Fixed invalid version number

##1.5 (2018-05-03)
### New modules
- Added filter modules
- Added translations module
- Added favicon counter module
- Added product variants module
- Added read more category description module
- Added product image hover module
- Added findify module

### Tools for development
- Added http-server (Node package) for serving files over localhost
- Removed the production part of the base.master script block.

### Improvements and general issues
- Added class product-list to startpage object ul
- Added mixin for adaptive image containers and implemented on cat, prod and checkout pages
- Added bottom-margin with the same value as margin-right for the calculate-item-width mixinfor elements to not touch each other
- Added the forgotten currency spacer on product page
- Added promotion currency spacer margin
- Added sass breakpoints to the built file
- Added classFriendly helper for handlebars
- Improved the general design of search page
- Improved the login flow on mobile devices
- Improved customer club design on order confirmation page
- Stopped the erroneous removal of attribute parameter wrapper
- Stop dynamic cart btn from triggering postback under some circumstances
- Fix for images overflowing item box on uppermost range of small size in checkout
- Fix for Handlebars unescape helper that bugged out in IE
- Fix for html entities being displayed on comments in dynamic cart
- Resolved issue #87, Add page ID body class for standard pages
- Hide previous price unit as standard.
- Remove overlay images

##1.4.6 (2017-06-29)
- Added Nosto to Searchpage in the nosto module
- Added channelInfo to J.data
- Added a clearOutdatedStorage function which clears outdated localStorage posts.
- Added support for product-overlays being cleaner on smaller devices
- Added function to hide nosto content if no products are shown
- Added a setting to override text-rendering to optimizeLegibility if neccessary. Performance issues were identified.
- Minor fixes with Jetshop logo not being shown in other channels

##1.4.5 (2017-03-21)
- Added Foundation Interchange
- Added Slick Slider module
- Added Handling of portnumber to J.api.helpers.baseUrl
- Added Sass Responsive Breakpoints to JS (J.config.breakpoints)
- Minor fixes with differencies of gulp and grunt buildprocess

##1.4.4 (2017-03-03)
- Added script-handling.js to solve stage/production separation for scripts
- Added file-preview for modules in Nodeserver
- Added a better skeleton structure when creating modules in Nodeserver
- Added J.components.addToCart()
- Sorted a Windows issue with path separators
- Module dynamic-cart will now disable "To cart" button until postback is done
- Removed unnecessary window.resize in J.init, added dependencies between cat-nav-mobile & mobile-menu
- Added missing j-flow tags: <j:ProductInCampaignsHeader/> & <j:ProductInCampaignsList/>
- Added try/catch to getUrlParameters to avoid possible error when filtering on strings containing "åäö"
- Added estonian translations to dynamic-cart

##1.4.3 (2017-02-10)
- Added handling of dynamic url's
- Updated bowser to detect MS Surface
- Resolved issue #67, Fancybox buttons within cat-nav does not trigger fancybox

##v1.4.1 (2017-01-25)
- Added gulp + browserSync for customers with FTP Secure
- Resolved issue #38, hide breadcrumbs on checkout page
- Resolved issue #54, hide pagination-numbers for device width small
- Resolved issue #62, culture parameter missing in J.api.category.get
- Resolved issue #65, product are highlighted when removing from dynamic-cart
- Resolved issue #66, J.switch queues were not triggered on page load

##v1.4.0 (2016-12-01)
- Added ability to export standalone modules from Nodeserver
- Added module Nosto (Recommendations)
- Added module Yotpo (Ratings & reviews)
- Added ifCond helper to Handlebars
- Added removeEmptyAttributeWrappers
- Added removeEmptySpecificationWrapper
- Resolved issue #60, cat-nav-mobile will hide LV2-links if list is too long
- Resolved issue #58, fixes for currency & culture selector
- Changed default culture-selector to display flags

##v1.3.0 (2016-07-01)
- Added SASS file for Responsive Order Confirm
- Improved handling of pagination, global mixins used
- Improved default style for checkout
- Previous price is now consistently hidden on list products as well as startpage objects
- Product images in checkout now has a fixed height in device width small
- Moved calls to J.components from base-responsive-core.js to client.js
- Overall major improvements to design handling and default look
- Several issues and minor improvements implemented

##v1.2.0 (2016-05-17)
- Updated Foundation to version 5.5.3, more info here: http://foundation.zurb.com/sites/docs/v/5.5.3/changelog.html
- Major refactoring for handling of buttons, see file _buttons-v2.scss
- Using JIT in Gruntfile to speed up build process
- Removed deprecated module "add-to-cart-popup" since it's replaced by "dynamic-cart"
- Updated & refactored tests.
- Added possibility to add tests from modules
- Added test for module "dynamic-cart"
- Added event for dynamic-cart-closed
- Added setting to display custom pages, VAT-, culture- and currency-selector to mobile menu
- Added gray-scale function to mixins
- Added file _typography.scss from Foundation
- Resolved issue #36, PC & Mac treats line-feeds in Handlebars differently

##v1.1.1 (2016-05-16)
- Minor bugfix release
- Sorted bug in startPageObjects
- Sorted IE-related bug in J.api.helpers.baseUrl

##v1.1.0 (2016-04-12)
- Moved J.menu into modules for separation. File responsive-base-core.js are now prepared to be a server-hosted library.
- Added prototype for editing SASS in Nodeserver. This is a preparation for future Quick-design functionality.
- Cat-nav and mobile menu are now the same height. Resolving issue #22.
- Added Selenium-tests. Tests are working, but have to be run against prepared site foundation2.jetshop.se. This behaviour will change.
- Added module get-related-products: will list related products using Handlebars.
- Added module breadcrumbs: will add link to home-page in breadcrumbs.
- Added module subcategories: will add links to sub-categories.
- Added emptyCart function to J.components.

##v1.0.0 (2016-03-22)
- Major change: client-specific development are now done in file client.js
- Renamed 'responsive-base.js' to 'responsive-base-core.js.' This file is now treated as a library
- Major changes to folder structure: folder 'core' now contains files that build to folder 'stage'
- Added 3 modules to handle category-navigation on desktop
- Added module 'mobile-cat-nav' for category-navigation on smaller screens
- Replaced old module 'add-to-cart-popup' with module 'dynamic-cart'
- J.cart now contains way more information on cart content
- Grunt-tasks for uploading are now separated to speed up development
- Renamed 'templates' to 'pages' for consistency, and to avoid confusion with Handlebars-views
- Handlebars-views now use file extension .hbs for better compatibility with editors
- Replaced Foundation.utils.helper with internal logic for switch detection
- Added JS-events for cart-loaded, cart-updated and device-width switch
- J.api now accepts objects as callback, honoring AJAX-methods before, error, success & complete
- J.api.product.get now accepts int as well as array
- Added 'getRelatedProducts' to  J.api.product
- Added startpages list & get to J.api
- Resolved bug with J.api & quota-exceeded errors
- Resolved bug with Android 4.3 rendering white pages
- Resolved bug with responsive order confirm being wrongfully identified as "checkout-page"
- Resolved bug in setCookie to make it possible to set session cookies without specifying expiry time.
- Resolved bug with disappearing myPages-link
- Prevent J.init() from firing twice, resolves issue #32.
- Added Node server for listing of module, as well as debugging
- Updated Font Awesome to version 4.5

##v0.8.6
- Minor bugfixes: #29, #32, #31, #25

##v0.8.5
- Project renamed to "Responsive Base"
- Added J.api to get an uniform interface for fetching REST-data
- Added optional caching of REST-data to Local Storage
- Added JS templating engine "Handlebars". Templates are exposed as J.views
- Added several helpers for Handlebars to handle JetshopData (translate, currentLanguage, currentCulture)
- Added jquery-match-height to replace setSameHeight() functionality
- Added grunt-focus to enable targets in grunt-contrib-watch

##v0.8.4
- Refactored handling of JetshopData & jlib. J.checker are now booleans only, all other Jetshop-specific data are moved into J.data

##v0.8.3
- Major changes to handling of pageType functionality, all initPageTypeXX functions are now gone and replaced by J.pages
- J.pages accept queues: ``J.pages.addToQueue("page-default",myCustomFunction);`` This will make modules more independent

##v0.8.2
- All functions per pageType that previously were private are now bundled in J.components
- Major changes to functions that are triggered by changes to device width. J.initSwitchToSmall are gone and replaced by J.switch. J.switch accepts queues: ``J.switch.addToSmall(checkMobileMenuStyles);``
- Major changes to handling of translations, J.translations are now an array that accept push: ;``J.translations.push({ translationsObjects});;``
- Added ``grunt noftp`` which is default tasks minus the FTP-handling. This will speed up development time when working locally (or from Brazil)
- Resolved issues caused by conflicting class-names with Foundation

##v0.8.1
- Resolved bug in initTabSystem()
- Resolved bug in startPageObjects()
- Resolved bug with input heights on iOS
- Major restructuring of SCSS
- Updated readme.md

##v0.8.0
- Added modules
- Added Universal Grid: layout, css and DOM are now identical for listProducts, startPageObjects and Releware
- Moved J.init() from template to script-file
- Added support for support.css, this file will not be handled by FTP deploy
- Resolved bug in J.setPageType regarding pages with productId & categoryId set to 0
- Numerous bugfixes and issues resolved
- Added functionality in responsive-base-libraries.js
- Added ``grunt package``
- Major update to readme's, documentation and support sites

##v0.7.0
- Added templates for Releware
- Added functionality to Gruntfile
- Added the entire Foundation SCSS package
- Bugfixes for iOS

