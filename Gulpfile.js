'use strict';
//
//  You should probably not use this file.
//  Gulp is for customers on FTP Secure (ftps)
//  Unless instructed, use grunt.
//
var gulp = require('gulp');
var fs = require("fs");
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat-util');
var ftp = require('vinyl-ftp');
var gutil = require('gulp-util');
var handlebars = require('gulp-handlebars-all');
var declare = require('gulp-declare');
var del = require('del');
var zip = require('gulp-zip');
var streamqueue = require('streamqueue');
var path = require("path");
var dateFormat = require('dateformat');
var inject = require('gulp-inject-string');
var browserSync = require('browser-sync').create();

//  We use this to grab CSS breakpoints from SASS
var getSassBreakpoints = require("./tools/utils/getSassBreakpoints.js");

//  We use this to strip the module paths
function processModulePath(filePath) {
    var relativePath = declare.processNameByPath(filePath.replace('core/modules/'.replace(/\//g, path.sep), ''));
    return relativePath.replace('.hbs', '').replace('.views', '').replace('core/modules/', '').replace(/\./g, '/');
}

// Load custom paths
var paths = require("./gulp-config");

// Load ftp config
var ftpconfig = require('./.ftpconfig');

// Define tasks
gulp.task('sass:develop', function () {
    return gulp.src(paths.sass.src)
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'nested',
            debugInfo: true,
            trace: true,
            precision: 10,
            lineNumbers: true
        }).on('error', sass.logError))
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest(paths.sass.dest));
});

gulp.task('sass:production', function () {
    return gulp.src(paths.sass.src)
        .pipe(sass({
            outputStyle: 'nested',
            precision: 10
        }).on('error', sass.logError))
        .pipe(gulp.dest(paths.sass.dest));
});

gulp.task('views', function () {
    gulp.src(paths.views.src)
        .pipe(handlebars('js'))
        .pipe(declare({
            namespace: paths.views.nameSpace,
            noRedeclare: true,
            processName: function (filePath) {
                return processModulePath(filePath)
            }
        }))
        .pipe(concat(paths.views.fileName, {sep: "\n\n"}))
        .pipe(inject.before('this["J"]["views"][', "\n"))
        .pipe(gulp.dest(paths.views.dest));
});

gulp.task('scripts', function () {
    gulp.src(paths.scripts.src)
        .pipe(concat(paths.scripts.fileName, {
            process: function (src, filepath) {
                var pathArray = filepath.split(path.sep);
                return '\n// \n//    Module: ' + pathArray[pathArray.length - 3] + '\n//\n' + src;
            }
        }))
        .pipe(inject.prepend(getSassBreakpoints() + "\n"))
        .pipe(gulp.dest(paths.scripts.dest));
});

gulp.task('clean', function () {
    return del([
        "**/.DS_Store",
        "**/ehthumbs.db",
        "**/.Spotlight-V100",
        "**/Thumbs.db"
    ]);
});

gulp.task("compress", function () {
    var timestamp = dateFormat(new Date(), "yyyy-mm-dd_HH.MM.ss");
    var stream = streamqueue({objectMode: true});
    stream.queue(gulp.src(["stage/scripts/**/**"], {base: "stage/"}));
    stream.queue(gulp.src(["stage/images/**/**"], {base: "stage/"}));
    stream.queue(gulp.src("stage/css/**/**", {base: "stage/"}));
    stream.queue(gulp.src("core/pages/**/**", {base: "core/pages/"}));
    return stream.done()
        .pipe(zip("package_" + timestamp + ".zip"))
        .pipe(gulp.dest("core/package/"));
});

gulp.task('watch', function () {
    gulp.watch(paths.sassWatch, ['sass:develop']);
    gulp.watch(paths.views.src, ['views']);
    gulp.watch(paths.scripts.src, ['scripts']);
});

gulp.task('upload', function () {
    var conn = ftp.create(ftpconfig);
    var globs = paths.upload.globs;
    return gulp.src(globs, {base: paths.upload.base, buffer: false})
        .pipe(conn.newer(paths.upload.dest))
        .pipe(conn.dest(paths.upload.dest))
        .on('end', function () {
            browserSync.reload();
            gutil.log(gutil.colors.white.bgCyan('> Upload finished'));
        });
});

gulp.task('watch-upload', function () {
    gulp.watch(paths.watch.upload, ['upload']);
});

gulp.task('browser-sync', [], function () {
    browserSync.init({
        proxy: ftpconfig.shopAddress,
        port: 4000,
        ui: {
            port: 4001
        },
        weinre: {
            port: 4002
        }
    });
});

// Define shortcuts
gulp.task('default', ['sass:develop', 'views', 'scripts', 'watch', 'watch-upload']);
gulp.task('production', ['sass:production', 'views', 'scripts', 'watch-upload']);
gulp.task('noftp', ['sass:develop', 'views', 'scripts', 'watch']);
gulp.task('ftpsync', ['upload']);
gulp.task('package', ['clean', 'compress']);
gulp.task('sync', ['default', 'browser-sync']);
