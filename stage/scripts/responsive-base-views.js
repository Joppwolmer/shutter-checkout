this["J"] = this["J"] || {};
this["J"]["views"] = this["J"]["views"] || {};

this["J"]["views"]["dynamic-cart/dynamic-cart-item"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"dc-item-row "
    + ((stack1 = (helpers.ifValue || (depth0 && depth0.ifValue) || alias2).call(alias1,(depth0 != null ? depth0.ProductId : depth0),{"name":"ifValue","hash":{"equals":(depths[1] != null ? depths[1].lastProductAdded : depths[1])},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" data-id=\""
    + alias4(((helper = (helper = helpers.ProductId || (depth0 != null ? depth0.ProductId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ProductId","hash":{},"data":data}) : helper)))
    + "\" data-rec-id=\""
    + alias4(((helper = (helper = helpers.RecId || (depth0 != null ? depth0.RecId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"RecId","hash":{},"data":data}) : helper)))
    + "\" data-attribute-id=\"-1\">\n    <div class=\"img\">\n"
    + ((stack1 = helpers["if"].call(alias1,((stack1 = ((stack1 = (depth0 != null ? depth0.Images : depth0)) != null ? stack1["0"] : stack1)) != null ? stack1.Url : stack1),{"name":"if","hash":{},"fn":container.program(4, data, 0, blockParams, depths),"inverse":container.program(7, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "    </div>\n    <div class=\"item\">\n        <div class=\"description\">\n            <a class=\"name\" href=\""
    + ((stack1 = helpers["if"].call(alias1,(helpers.isStage || (depth0 && depth0.isStage) || alias2).call(alias1,{"name":"isStage","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(5, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + alias4(((helper = (helper = helpers.ProductLink || (depth0 != null ? depth0.ProductLink : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ProductLink","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + "</a>\n            <span class=\"item-number\">"
    + alias4((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcItemNumber",{"name":"translate","hash":{},"data":data}))
    + ": "
    + alias4(((helper = (helper = helpers.Articlenumber || (depth0 != null ? depth0.Articlenumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Articlenumber","hash":{},"data":data}) : helper)))
    + "</span>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.Comments : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </div>\n    </div>\n    <div class=\"price\">\n        <span class=\"price-string\">\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded || (depth0 && depth0.isVatIncluded) || alias2).call(alias1,{"name":"isVatIncluded","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(12, data, 0, blockParams, depths),"inverse":container.program(14, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "        </span>\n        <span class=\"separator\"> / </span>\n        <span class=\"qty-suffix\">"
    + alias4(((helper = (helper = helpers.QuantitySuffix || (depth0 != null ? depth0.QuantitySuffix : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"QuantitySuffix","hash":{},"data":data}) : helper)))
    + "</span>\n    </div>\n    <div class=\"qty\">\n        <span class=\"label\">"
    + alias4((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcQty",{"name":"translate","hash":{},"data":data}))
    + ": </span>\n        <span class=\"value\">"
    + alias4(((helper = (helper = helpers.Quantity || (depth0 != null ? depth0.Quantity : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Quantity","hash":{},"data":data}) : helper)))
    + "</span>\n    </div>\n    <div class=\"delete\">\n        <i class=\"fa fa-times-circle\"></i>\n    </div>\n    <div class=\"total\">\n        <span>\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded || (depth0 && depth0.isVatIncluded) || alias2).call(alias1,{"name":"isVatIncluded","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(16, data, 0, blockParams, depths),"inverse":container.program(18, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "        </span>\n    </div>\n</div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "product-added";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <a href=\""
    + ((stack1 = helpers["if"].call(alias1,(helpers.isStage || (depth0 && depth0.isStage) || alias2).call(alias1,{"name":"isStage","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + alias4(((helper = (helper = helpers.ProductLink || (depth0 != null ? depth0.ProductLink : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ProductLink","hash":{},"data":data}) : helper)))
    + "\"><img src=\""
    + alias4(container.lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.Images : depth0)) != null ? stack1["0"] : stack1)) != null ? stack1.Url : stack1), depth0))
    + "\" alt=\""
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + "\"></a>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "/stage";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <a href=\""
    + ((stack1 = helpers["if"].call(alias1,(helpers.isStage || (depth0 && depth0.isStage) || alias2).call(alias1,{"name":"isStage","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + alias4(((helper = (helper = helpers.ProductLink || (depth0 != null ? depth0.ProductLink : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ProductLink","hash":{},"data":data}) : helper)))
    + "\"><img src=\"/m1/stage/images/responsive-base/missing-image.png\" alt=\""
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + "\"></a>\n";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "            <div class=\"comments\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.Comments : depth0),{"name":"each","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n";
},"10":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3=container.escapeExpression;

  return "                    <div class=\"comment\">\n                        <span class='key'>"
    + alias3(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + ":</span> <span class=\"value\">"
    + alias3((helpers.unescape || (depth0 && depth0.unescape) || alias2).call(alias1,(depth0 != null ? depth0.Comment : depth0),{"name":"unescape","hash":{},"data":data}))
    + "</span>\n                    </div>\n";
},"12":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.ItemPriceSumText || (depth0 != null ? depth0.ItemPriceSumText : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"ItemPriceSumText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"14":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.ItemPriceSumNoVatText || (depth0 != null ? depth0.ItemPriceSumNoVatText : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"ItemPriceSumNoVatText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"16":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.TotalPriceSumText || (depth0 != null ? depth0.TotalPriceSumText : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"TotalPriceSumText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"18":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.TotalPriceSumNoVatText || (depth0 != null ? depth0.TotalPriceSumNoVatText : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"TotalPriceSumNoVatText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.ProductsCart : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true,"useDepths":true});

this["J"]["views"]["dynamic-cart/dynamic-cart-totals"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.TotalProductSumText || (depth0 != null ? depth0.TotalProductSumText : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"TotalProductSumText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.TotalProductSumNoVatText || (depth0 != null ? depth0.TotalProductSumNoVatText : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"TotalProductSumNoVatText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing;

  return "        <div class=\"vat-total\">\n            <span class=\"label\">\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded || (depth0 && depth0.isVatIncluded) || alias2).call(alias1,{"name":"isVatIncluded","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.program(8, data, 0),"data":data})) != null ? stack1 : "")
    + "            </span>\n            <span class=\"value\">"
    + ((stack1 = ((helper = (helper = helpers.TotalProductVatSumText || (depth0 != null ? depth0.TotalProductVatSumText : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"TotalProductVatSumText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\n        </div>\n";
},"6":function(container,depth0,helpers,partials,data) {
    return "                "
    + container.escapeExpression((helpers.translate || (depth0 && depth0.translate) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"dcOfWhichVat",{"name":"translate","hash":{},"data":data}))
    + "\n";
},"8":function(container,depth0,helpers,partials,data) {
    return "                "
    + container.escapeExpression((helpers.translate || (depth0 && depth0.translate) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"dcPlusVat",{"name":"translate","hash":{},"data":data}))
    + "\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing;

  return "<div id=\"dc-totals\">\n    <div class=\"sum-total\">\n        <span class=\"label\">"
    + container.escapeExpression((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcItemTotal",{"name":"translate","hash":{},"data":data}))
    + ":</span>\n        <span class=\"value\">\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded || (depth0 && depth0.isVatIncluded) || alias2).call(alias1,{"name":"isVatIncluded","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "        </span>\n    </div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.showTotalVat : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true});

this["J"]["views"]["dynamic-cart/dynamic-cart"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3=container.escapeExpression;

  return "<div id='dc-wrapper' class='reveal-modal' data-reveal data-options=\"close_on_background_click:true\">\n    <div id=\"dc-inner\">\n        <h3 id=\"dc-header\">"
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcCartHeader",{"name":"translate","hash":{},"data":data}))
    + "</h3>\n        <div id=\"dc-body\">\n            <div id=\"dc-items-header\" class=\"clearfix\">\n                <div class=\"img\">&nbsp;</div>\n                <div class=\"item\">"
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcItem",{"name":"translate","hash":{},"data":data}))
    + "</div>\n                <div class=\"price\">"
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcPrice",{"name":"translate","hash":{},"data":data}))
    + "</div>\n                <div class=\"qty\">"
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcQty",{"name":"translate","hash":{},"data":data}))
    + "</div>\n                <div class=\"delete\">&nbsp;</div>\n                <div class=\"total\">"
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcTotal",{"name":"translate","hash":{},"data":data}))
    + "</div>\n            </div>\n            <div id=\"dc-content\">\n                <!-- Cart content goes here, populated from view dynamic-cart-item -->\n            </div>\n            <div id=\"dc-loader-msg\"><span class=\"add-prod\">"
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcProdLoadMsg",{"name":"translate","hash":{},"data":data}))
    + "</span></div>\n            <div id=\"dc-totals\">\n            </div>\n        </div>\n        <div id=\"dc-btns\">\n            <button id=\"dc-checkout-btn\" class=\"button\" href=\""
    + alias3((helpers.config || (depth0 && depth0.config) || alias2).call(alias1,"urls.CheckoutUrl",{"name":"config","hash":{},"data":data}))
    + "\"><span>"
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcCheckoutBtnText",{"name":"translate","hash":{},"data":data}))
    + "</span></button>\n            <button id=\"dc-continue-btn\" class=\"button\" href=\"\"><span>"
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"dcContinueBtnText",{"name":"translate","hash":{},"data":data}))
    + "</span></button>\n        </div>\n    </div>\n</div>\n";
},"useData":true});

this["J"]["views"]["dynamic-filter-core/dynamic-filter-core"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "";
},"useData":true});

this["J"]["views"]["dynamic-filter-gui-top/dynamic-filter-options-desktop"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <div data-equalizer-watch class=\"filter-options-item filter-option--"
    + alias4(((helper = (helper = helpers.Type || (depth0 != null ? depth0.Type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Type","hash":{},"data":data}) : helper)))
    + " filter-option--"
    + alias4((helpers.classFriendly || (depth0 && depth0.classFriendly) || alias2).call(alias1,(depth0 != null ? depth0.Key : depth0),{"name":"classFriendly","hash":{},"data":data}))
    + "\">\n                <div class=\""
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.Type : depth0),"!=","multiLevelList",{"name":"ifCond","hash":{},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.program(4, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + " filter-option-header\">\n                    <h4>"
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + "</h4>\n                </div>\n\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.Type : depth0),"===","list",{"name":"ifCond","hash":{},"fn":container.program(6, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.Type : depth0),"===","multiLevelList",{"name":"ifCond","hash":{},"fn":container.program(12, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.Type : depth0),"===","span",{"name":"ifCond","hash":{},"fn":container.program(17, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.Type : depth0),"===","bool",{"name":"ifCond","hash":{},"fn":container.program(19, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "hide";
},"4":function(container,depth0,helpers,partials,data) {
    return "show-for-medium-down";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                    <div data-type=\""
    + alias4(((helper = (helper = helpers.Type || (depth0 != null ? depth0.Type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Type","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + "\" data-key=\""
    + alias4(((helper = (helper = helpers.Key || (depth0 != null ? depth0.Key : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Key","hash":{},"data":data}) : helper)))
    + "\"\n                         class=\"filter-option-element filter-option-element--"
    + alias4(((helper = (helper = helpers.Type || (depth0 != null ? depth0.Type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Type","hash":{},"data":data}) : helper)))
    + " filter-option-element--"
    + alias4((helpers.classFriendly || (depth0 && depth0.classFriendly) || alias2).call(alias1,(depth0 != null ? depth0.Key : depth0),{"name":"classFriendly","hash":{},"data":data}))
    + "\">\n                        <select multiple>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.Values : depth0),{"name":"each","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                        </select>\n                    </div>\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                                <option title=\""
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + " "
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.Selected : depth0),{"name":"unless","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" value=\""
    + alias4(((helper = (helper = helpers.Value || (depth0 != null ? depth0.Value : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Value","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.Selected : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\n                                    "
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + " "
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.Selected : depth0),{"name":"unless","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n                                </option>\n";
},"8":function(container,depth0,helpers,partials,data) {
    var helper;

  return "("
    + container.escapeExpression(((helper = (helper = helpers.ProductCount || (depth0 != null ? depth0.ProductCount : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"ProductCount","hash":{},"data":data}) : helper)))
    + ")";
},"10":function(container,depth0,helpers,partials,data) {
    return "selected=\"selected\"";
},"12":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.GroupFilters : depth0),{"name":"each","hash":{},"fn":container.program(13, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"13":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.Type : depth0),"===","list",{"name":"ifCond","hash":{},"fn":container.program(14, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"14":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=container.escapeExpression, alias2=depth0 != null ? depth0 : (container.nullContext || {}), alias3=helpers.helperMissing, alias4="function";

  return "                            <div data-type=\""
    + alias1(container.lambda((depths[1] != null ? depths[1].Type : depths[1]), depth0))
    + "\" data-name=\""
    + alias1(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"Name","hash":{},"data":data}) : helper)))
    + "\" data-key=\""
    + alias1(((helper = (helper = helpers.Key || (depth0 != null ? depth0.Key : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"Key","hash":{},"data":data}) : helper)))
    + "\"\n                                 class=\"filter-option-element filter-option-element--"
    + alias1(((helper = (helper = helpers.Type || (depth0 != null ? depth0.Type : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"Type","hash":{},"data":data}) : helper)))
    + " filter-option-element--"
    + alias1((helpers.classFriendly || (depth0 && depth0.classFriendly) || alias3).call(alias2,(depth0 != null ? depth0.Key : depth0),{"name":"classFriendly","hash":{},"data":data}))
    + "\">\n                                <select name=\""
    + alias1((helpers.classFriendly || (depth0 && depth0.classFriendly) || alias3).call(alias2,(depth0 != null ? depth0.Key : depth0),{"name":"classFriendly","hash":{},"data":data}))
    + "\">\n                                    <option value=\"-1\">"
    + alias1(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"Name","hash":{},"data":data}) : helper)))
    + "</option>\n"
    + ((stack1 = helpers.each.call(alias2,(depth0 != null ? depth0.Values : depth0),{"name":"each","hash":{},"fn":container.program(15, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                                </select>\n                            </div>\n";
},"15":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                                        <option title=\""
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + " "
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.Selected : depth0),{"name":"unless","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" value=\""
    + alias4(((helper = (helper = helpers.Value || (depth0 != null ? depth0.Value : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Value","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.Selected : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\n                                            "
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + " ("
    + alias4(((helper = (helper = helpers.ProductCount || (depth0 != null ? depth0.ProductCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ProductCount","hash":{},"data":data}) : helper)))
    + ")\n                                        </option>\n";
},"17":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "                    <div data-type=\""
    + alias4(((helper = (helper = helpers.Type || (depth0 != null ? depth0.Type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Type","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + "\" data-key=\""
    + alias4(((helper = (helper = helpers.Key || (depth0 != null ? depth0.Key : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Key","hash":{},"data":data}) : helper)))
    + "\"\n                         class=\"filter-option-element filter-option-element--"
    + alias4(((helper = (helper = helpers.Type || (depth0 != null ? depth0.Type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Type","hash":{},"data":data}) : helper)))
    + " filter-option-element--"
    + alias4((helpers.classFriendly || (depth0 && depth0.classFriendly) || alias2).call(alias1,(depth0 != null ? depth0.Key : depth0),{"name":"classFriendly","hash":{},"data":data}))
    + "\">\n                        <div class=\"filter-option-box-button\">\n                            "
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + "\n                        </div>\n\n                        <div class=\"filter-option-box-content\">\n                            <div class=\"filter-option-box-content-input\">\n                                <div class=\"span-input\" data-min=\""
    + alias4(alias5(((stack1 = ((stack1 = (depth0 != null ? depth0.Values : depth0)) != null ? stack1["0"] : stack1)) != null ? stack1.Value : stack1), depth0))
    + "\"\n                                     data-max=\""
    + alias4(alias5(((stack1 = ((stack1 = (depth0 != null ? depth0.Values : depth0)) != null ? stack1["1"] : stack1)) != null ? stack1.Value : stack1), depth0))
    + "\"></div>\n                            </div>\n\n                            <button type=\"button\" class=\"button btnOk\">\n                                "
    + alias4((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"FilterOk",{"name":"translate","hash":{},"data":data}))
    + "\n                            </button>\n\n                            <button type=\"button\" class=\"button btnClose\">\n                                "
    + alias4((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"FilterClose",{"name":"translate","hash":{},"data":data}))
    + "\n                            </button>\n                        </div>\n                    </div>\n";
},"19":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                    <div data-type=\""
    + alias4(((helper = (helper = helpers.Type || (depth0 != null ? depth0.Type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Type","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + "\" data-key=\""
    + alias4(((helper = (helper = helpers.Key || (depth0 != null ? depth0.Key : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Key","hash":{},"data":data}) : helper)))
    + "\"\n                         class=\"filter-option-element filter-option-element--"
    + alias4(((helper = (helper = helpers.Type || (depth0 != null ? depth0.Type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Type","hash":{},"data":data}) : helper)))
    + " filter-option-element--"
    + alias4((helpers.classFriendly || (depth0 && depth0.classFriendly) || alias2).call(alias1,(depth0 != null ? depth0.Key : depth0),{"name":"classFriendly","hash":{},"data":data}))
    + "\">\n                        <div class=\"filter-option-box-button\">\n                            "
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + "\n                        </div>\n\n                        <div class=\"filter-option-box-content\">\n                            <div class=\"filter-option-box-content-input\">\n                                <input type=\"checkbox\" value=\"1\" "
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,((stack1 = ((stack1 = (depth0 != null ? depth0.Values : depth0)) != null ? stack1["0"] : stack1)) != null ? stack1.Selected : stack1),"==",true,{"name":"ifCond","hash":{},"fn":container.program(20, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " id=\"desktop-filter-"
    + alias4((helpers.classFriendly || (depth0 && depth0.classFriendly) || alias2).call(alias1,(depth0 != null ? depth0.Key : depth0),{"name":"classFriendly","hash":{},"data":data}))
    + "\">\n                                <label for=\"desktop-filter-"
    + alias4((helpers.classFriendly || (depth0 && depth0.classFriendly) || alias2).call(alias1,(depth0 != null ? depth0.Key : depth0),{"name":"classFriendly","hash":{},"data":data}))
    + "\">"
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + "</label>\n                            </div>\n\n                            <button type=\"button\" class=\"button btnOk\">\n                                "
    + alias4((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"FilterOk",{"name":"translate","hash":{},"data":data}))
    + "\n                            </button>\n\n                            <button type=\"button\" class=\"button btnClose\">\n                                "
    + alias4((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"FilterClose",{"name":"translate","hash":{},"data":data}))
    + "\n                            </button>\n                        </div>\n                    </div>\n";
},"20":function(container,depth0,helpers,partials,data) {
    return "checked";
},"22":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.Type : depth0),"==","multiLevelList",{"name":"ifCond","hash":{},"fn":container.program(23, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.Values : depth0),{"name":"each","hash":{},"fn":container.program(28, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"23":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <span class=\"filter-choices-item filter-choice-multilevel-items action--remove-filter-choice\" data-type=\""
    + alias4(((helper = (helper = helpers.Type || (depth0 != null ? depth0.Type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Type","hash":{},"data":data}) : helper)))
    + "\" data-key=\""
    + alias4(((helper = (helper = helpers.Key || (depth0 != null ? depth0.Key : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Key","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + "\">\n                "
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper)))
    + "<br class=\"show-for-small-only\" />\n                <div class=\"filter-choices-multilevel-selected\"> \n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.GroupFilters : depth0),{"name":"each","hash":{},"fn":container.program(24, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </div>\n                <span class=\"filter-choices-item--remove\">\n                    <i class=\"fa fa-close\"></i>\n                </span>            \n            </span>\n";
},"24":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.Values : depth0),{"name":"each","hash":{},"fn":container.program(25, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"25":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "                            "
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.Selected : depth0),{"name":"if","hash":{},"fn":container.program(26, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n";
},"26":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<span>"
    + container.escapeExpression(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"Name","hash":{},"data":data}) : helper)))
    + "</span>";
},"28":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depths[1] != null ? depths[1].Type : depths[1]),"===","span",{"name":"ifCond","hash":{},"fn":container.program(29, data, 0, blockParams, depths),"inverse":container.program(36, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "");
},"29":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.SelectedValue : depth0),"!==",null,{"name":"ifCond","hash":{},"fn":container.program(30, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"30":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : (container.nullContext || {}), alias4=helpers.helperMissing, alias5="function";

  return "                    <span class=\"filter-choices-item action--remove-filter-choice\" data-type=\""
    + alias2(alias1((depths[1] != null ? depths[1].Type : depths[1]), depth0))
    + "\" data-key=\""
    + alias2(alias1((depths[1] != null ? depths[1].Key : depths[1]), depth0))
    + "\" data-selectedValue=\""
    + alias2(((helper = (helper = helpers.SelectedValue || (depth0 != null ? depth0.SelectedValue : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"SelectedValue","hash":{},"data":data}) : helper)))
    + "\" data-value=\""
    + alias2(((helper = (helper = helpers.Value || (depth0 != null ? depth0.Value : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"Value","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias2(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"Name","hash":{},"data":data}) : helper)))
    + "\">\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias4).call(alias3,(depths[1] != null ? depths[1].Type : depths[1]),"==","span",{"name":"ifCond","hash":{},"fn":container.program(31, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                        <span class=\"filter-choices-item--remove\">\n                            <i class=\"fa fa-close\"></i>\n                        </span>\n                    </span>\n";
},"31":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing;

  return ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.Name : depth0),"==","pricemin",{"name":"ifCond","hash":{},"fn":container.program(32, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.Name : depth0),"==","pricemax",{"name":"ifCond","hash":{},"fn":container.program(34, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"32":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3=container.escapeExpression;

  return "                                "
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"FilterMinPrice",{"name":"translate","hash":{},"data":data}))
    + " "
    + alias3((helpers.currency || (depth0 && depth0.currency) || alias2).call(alias1,(depth0 != null ? depth0.SelectedValue : depth0),{"name":"currency","hash":{},"data":data}))
    + "\n";
},"34":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3=container.escapeExpression;

  return "                                "
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"FilterMaxPrice",{"name":"translate","hash":{},"data":data}))
    + " "
    + alias3((helpers.currency || (depth0 && depth0.currency) || alias2).call(alias1,(depth0 != null ? depth0.SelectedValue : depth0),{"name":"currency","hash":{},"data":data}))
    + "\n";
},"36":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.Selected : depth0),{"name":"if","hash":{},"fn":container.program(37, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"37":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : (container.nullContext || {}), alias4=helpers.helperMissing, alias5="function";

  return "                    <span class=\"filter-choices-item action--remove-filter-choice\" data-type=\""
    + alias2(alias1((depths[1] != null ? depths[1].Type : depths[1]), depth0))
    + "\" data-key=\""
    + alias2(alias1((depths[1] != null ? depths[1].Key : depths[1]), depth0))
    + "\" data-selectedValue=\""
    + alias2(((helper = (helper = helpers.SelectedValue || (depth0 != null ? depth0.SelectedValue : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"SelectedValue","hash":{},"data":data}) : helper)))
    + "\" data-value=\""
    + alias2(((helper = (helper = helpers.Value || (depth0 != null ? depth0.Value : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"Value","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias2(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"Name","hash":{},"data":data}) : helper)))
    + "\">\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias4).call(alias3,(depths[1] != null ? depths[1].Type : depths[1]),"==","bool",{"name":"ifCond","hash":{},"fn":container.program(38, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias4).call(alias3,(depths[1] != null ? depths[1].Type : depths[1]),"==","list",{"name":"ifCond","hash":{},"fn":container.program(40, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n                        <span class=\"filter-choices-item--remove\">\n                            <i class=\"fa fa-close\"></i>\n                        </span>\n                    </span>\n";
},"38":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var alias1=container.escapeExpression;

  return "                            "
    + alias1(container.lambda((depths[1] != null ? depths[1].Name : depths[1]), depth0))
    + ": "
    + alias1((helpers.translate || (depth0 && depth0.translate) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"FilterYes",{"name":"translate","hash":{},"data":data}))
    + "\n";
},"40":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var helper, alias1=container.escapeExpression;

  return "                            "
    + alias1(container.lambda((depths[1] != null ? depths[1].Name : depths[1]), depth0))
    + ": "
    + alias1(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"Name","hash":{},"data":data}) : helper)))
    + "\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3=container.escapeExpression;

  return "<h2 class=\"show-for-medium-up\">Filter</h2>\n<button type=\"button\" class=\"show-for-medium-down button action--show-filter\">\n    <i class=\"fa fa-sliders\"></i>\n    "
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"FilterMobileShowFilter",{"name":"translate","hash":{},"data":data}))
    + "\n</button>\n<div class=\"filter-container\">\n    <div class=\"filter-options\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.FilterOptions : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n\n    <div class=\"filter-actions show-for-medium-down\">\n        <button type=\"button\" class=\"button action--apply-filter\">\n            "
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"FilterApplyFilter",{"name":"translate","hash":{},"data":data}))
    + "\n        </button>\n        <button type=\"button\" class=\"button action--reset-filter\">\n            "
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"FilterReset",{"name":"translate","hash":{},"data":data}))
    + "\n        </button>\n    </div>\n</div>\n\n<div class=\"filter-choices\">\n    <h3>"
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"FilterYourChoices",{"name":"translate","hash":{},"data":data}))
    + "</h3>\n\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.FilterOptions : depth0),{"name":"each","hash":{},"fn":container.program(22, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    <span class=\"filter-choices-item filter-choices-item--reset action-reset-filter\">\n        "
    + alias3((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"FilterReset",{"name":"translate","hash":{},"data":data}))
    + "\n        <span>\n            <i class=\"fa fa-close\"></i>\n        </span>\n    </span>\n</div>";
},"useData":true,"useDepths":true});

this["J"]["views"]["dynamic-filter-gui-top/dynamic-filter-product-actions"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "		<button type=\"button\" class=\"button action--dynamicFilter-fetch-products\">\n			"
    + container.escapeExpression((helpers.translate || (depth0 && depth0.translate) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"FilterLoadMoreProducts",{"name":"translate","hash":{},"data":data}))
    + "\n		</button>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"dynamic-filter-product-actions\">\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.pageToFetch : depth0),"<=",(depth0 != null ? depth0.totalPages : depth0),{"name":"ifCond","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true});

this["J"]["views"]["dynamic-filter-gui-top/dynamic-filter-product-items-advanced"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "<li class=\"product-advanced-row\" data-id=\""
    + alias4(((helper = (helper = helpers.Id || (depth0 != null ? depth0.Id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Id","hash":{},"data":data}) : helper)))
    + "\">\n	<div class=\"product-advanced-image\">\n		<a title=\""
    + ((stack1 = ((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" href=\""
    + alias4(((helper = (helper = helpers.ProductUrl || (depth0 != null ? depth0.ProductUrl : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ProductUrl","hash":{},"data":data}) : helper)))
    + "\">\n			<img class=\"lazyimg\" title=\""
    + ((stack1 = ((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" data-original=\""
    + alias4(alias5(((stack1 = ((stack1 = (depth0 != null ? depth0.Images : depth0)) != null ? stack1["0"] : stack1)) != null ? stack1.Url : stack1), depth0))
    + "\" src=\""
    + alias4(alias5(((stack1 = ((stack1 = (depth0 != null ? depth0.Images : depth0)) != null ? stack1["0"] : stack1)) != null ? stack1.Url : stack1), depth0))
    + "\" alt=\""
    + ((stack1 = ((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\">\n			\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.ProductOverlays : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		</a>\n	</div>\n\n	<div class=\"product-advanced-column2\">\n		<div class=\"product-advanced-name\">\n			<a title=\""
    + ((stack1 = ((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" data-productid=\""
    + alias4(((helper = (helper = helpers.Id || (depth0 != null ? depth0.Id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Id","hash":{},"data":data}) : helper)))
    + "\" href=\""
    + alias4(((helper = (helper = helpers.ProductUrl || (depth0 != null ? depth0.ProductUrl : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ProductUrl","hash":{},"data":data}) : helper)))
    + "\">\n				"
    + ((stack1 = ((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n			</a>\n		</div>\n		<div class=\"product-advanced-subname\">\n			<span>"
    + ((stack1 = ((helper = (helper = helpers.SubName || (depth0 != null ? depth0.SubName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"SubName","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>    \n		</div>\n		<div class=\"product-advanced-article\">\n			<span>"
    + alias4(((helper = (helper = helpers.ArticleNumber || (depth0 != null ? depth0.ArticleNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ArticleNumber","hash":{},"data":data}) : helper)))
    + "</span>\n		</div>\n	</div>\n\n	<div class=\"product-advanced-column3\">\n		<div class=\"product-advanced-price-box\">\n"
    + ((stack1 = (helpers.ifValue || (depth0 && depth0.ifValue) || alias2).call(alias1,(depth0 != null ? depth0.DiscountedPrice : depth0),{"name":"ifValue","hash":{"equals":"-1"},"fn":container.program(6, data, 0),"inverse":container.program(11, data, 0),"data":data})) != null ? stack1 : "")
    + "			<div class=\"product-advanced-price-previous\">\n				<span>\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded || (depth0 && depth0.isVatIncluded) || alias2).call(alias1,{"name":"isVatIncluded","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.program(15, data, 0),"data":data})) != null ? stack1 : "")
    + "				</span>\n			</div>\n			<div class=\"product-advanced-price\">\n				<div class=\"price-promotion\">\n					<span>\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded || (depth0 && depth0.isVatIncluded) || alias2).call(alias1,{"name":"isVatIncluded","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(17, data, 0),"inverse":container.program(19, data, 0),"data":data})) != null ? stack1 : "")
    + "					</span> \n				</div>\n			</div>\n		</div>\n		<div class=\"product-advanced-price-stock-status\">\n			<span class=\"stock-status-label\">\n				<span>"
    + alias4((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"FilterStockStatus",{"name":"translate","hash":{},"data":data}))
    + ":</span>\n			</span> \n			<span class=\"stock-status-value\">\n				<span class=\"InStockCssClass\">"
    + alias4(((helper = (helper = helpers.StockStatusName || (depth0 != null ? depth0.StockStatusName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"StockStatusName","hash":{},"data":data}) : helper)))
    + "</span>\n			</span> \n		</div>\n\n			<div class=\"product-advanced-quantity\"></div>\n\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.Specifications : depth0)) != null ? stack1.length : stack1),"==",0,{"name":"ifCond","hash":{},"fn":container.program(21, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n			<div class=\"product-advanced-info-buttons\">\n				<a class=\"advanced-info-button-text\" href=\""
    + alias4(((helper = (helper = helpers.ProductUrl || (depth0 != null ? depth0.ProductUrl : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ProductUrl","hash":{},"data":data}) : helper)))
    + "\">\n					<span>"
    + alias4((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"infoButton",{"name":"translate","hash":{},"data":data}))
    + "</span>\n				</a>\n			</div>\n		</div>\n		<div class=\"product-advanced-description\">\n			"
    + ((stack1 = (helpers.unescape || (depth0 && depth0.unescape) || alias2).call(alias1,(depth0 != null ? depth0.ShortDescription : depth0),{"name":"unescape","hash":{},"data":data})) != null ? stack1 : "")
    + "\n		</div>\n	</div>\n</li>\n";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.ProductOverlays : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression, alias2=depth0 != null ? depth0 : (container.nullContext || {}), alias3=helpers.helperMissing, alias4="function";

  return "					<div class=\"pi-overlay pi-overlay-product-list pio-top-left pi-"
    + alias1(container.lambda(((stack1 = (data && data.root)) && stack1.Language), depth0))
    + "-overlay"
    + alias1(((helper = (helper = helpers.ImageIndex || (depth0 != null ? depth0.ImageIndex : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"ImageIndex","hash":{},"data":data}) : helper)))
    + " "
    + alias1(((helper = (helper = helpers.CssClass || (depth0 != null ? depth0.CssClass : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"CssClass","hash":{},"data":data}) : helper)))
    + "\">\n"
    + ((stack1 = helpers["if"].call(alias2,(depth0 != null ? depth0.OverlayTexts : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "					</div>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "							"
    + container.escapeExpression(container.lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.OverlayTexts : depth0)) != null ? stack1["0"] : stack1)) != null ? stack1.Value : stack1), depth0))
    + "\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "			<div class=\"product-advanced-price\">\n				<div class=\"price-standard\">\n					<span>\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded || (depth0 && depth0.isVatIncluded) || helpers.helperMissing).call(alias1,{"name":"isVatIncluded","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.program(9, data, 0),"data":data})) != null ? stack1 : "")
    + "					</span> \n				</div>\n			</div>\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "							"
    + ((stack1 = ((helper = (helper = helpers.PriceString || (depth0 != null ? depth0.PriceString : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"PriceString","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "							"
    + ((stack1 = ((helper = (helper = helpers.PriceStringWithoutVat || (depth0 != null ? depth0.PriceStringWithoutVat : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"PriceStringWithoutVat","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"11":function(container,depth0,helpers,partials,data) {
    return "";
},"13":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "						"
    + ((stack1 = ((helper = (helper = helpers.PriceString || (depth0 != null ? depth0.PriceString : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"PriceString","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"15":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "						"
    + ((stack1 = ((helper = (helper = helpers.PriceStringWithoutVat || (depth0 != null ? depth0.PriceStringWithoutVat : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"PriceStringWithoutVat","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"17":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "							"
    + ((stack1 = ((helper = (helper = helpers.DiscountedPriceString || (depth0 != null ? depth0.DiscountedPriceString : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"DiscountedPriceString","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"19":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "							"
    + ((stack1 = ((helper = (helper = helpers.DiscountedPriceStringWithoutVat || (depth0 != null ? depth0.DiscountedPriceStringWithoutVat : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"DiscountedPriceStringWithoutVat","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"21":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? depth0.Attributes : depth0)) != null ? stack1.length : stack1),"==",0,{"name":"ifCond","hash":{},"fn":container.program(22, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"22":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.IsBuyable : depth0),{"name":"if","hash":{},"fn":container.program(23, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"23":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "			<div class=\"product-advanced-price-buttons\">\n				<a class=\"advanced-buy-button-text\" onclick=\"AddItemToCart(this,'', '"
    + alias4(((helper = (helper = helpers.Id || (depth0 != null ? depth0.Id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Id","hash":{},"data":data}) : helper)))
    + "', '"
    + alias4(((helper = (helper = helpers.currentCulture || (depth0 != null ? depth0.currentCulture : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"currentCulture","hash":{},"data":data}) : helper)))
    + "', null);\"><span>"
    + alias4((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"buyButton",{"name":"translate","hash":{},"data":data}))
    + "</span></a>\n			</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.ProductItems : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true});

this["J"]["views"]["dynamic-filter-gui-top/dynamic-filter-product-items"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : (container.nullContext || {}), alias4=helpers.helperMissing, alias5="function";

  return "<li data-page=\""
    + alias2(alias1((depths[1] != null ? depths[1].CurrentPage : depths[1]), depth0))
    + "\">\n   <div class=\"product-outer-wrapper "
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias4).call(alias3,(depth0 != null ? depth0.IsBuyable : depth0),"!=",true,{"name":"ifCond","hash":{},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias4).call(alias3,(depth0 != null ? depth0.IsBuyable : depth0),"==",true,{"name":"ifCond","hash":{},"fn":container.program(5, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" product-id=\""
    + alias2(((helper = (helper = helpers.Id || (depth0 != null ? depth0.Id : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"Id","hash":{},"data":data}) : helper)))
    + "\">\n		<article class=\"product-wrapper\">\n			<div class=\"product-image\">\n				<a title=\""
    + alias2(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"Name","hash":{},"data":data}) : helper)))
    + "\" href=\""
    + alias2(((helper = (helper = helpers.ProductUrl || (depth0 != null ? depth0.ProductUrl : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"ProductUrl","hash":{},"data":data}) : helper)))
    + "\">\n					<img class=\"lazyimg\" title=\""
    + ((stack1 = ((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"Name","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" data-original=\""
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.Images : depth0)) != null ? stack1["0"] : stack1)) != null ? stack1.Url : stack1), depth0))
    + "\" src=\""
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.Images : depth0)) != null ? stack1["0"] : stack1)) != null ? stack1.Url : stack1), depth0))
    + "\"\n						 alt=\""
    + alias2(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"Name","hash":{},"data":data}) : helper)))
    + "\" style=\"display: block;\">\n"
    + ((stack1 = helpers["if"].call(alias3,(depth0 != null ? depth0.ProductOverlays : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "				</a>\n			</div>\n			<div class=\"product-buttons\">\n				<a class=\"button-info\" href=\""
    + alias2(((helper = (helper = helpers.ProductUrl || (depth0 != null ? depth0.ProductUrl : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"ProductUrl","hash":{},"data":data}) : helper)))
    + "\">\n					<span>"
    + alias2((helpers.translate || (depth0 && depth0.translate) || alias4).call(alias3,"infoButton",{"name":"translate","hash":{},"data":data}))
    + "</span>\n				</a>\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias4).call(alias3,((stack1 = (depth0 != null ? depth0.Specifications : depth0)) != null ? stack1.length : stack1),"==",0,{"name":"ifCond","hash":{},"fn":container.program(12, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "			</div>\n			<div class=\"product-info\">\n				<div class=\"product-name\">\n					<h3>\n						<a title=\""
    + ((stack1 = ((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"Name","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" data-productid=\""
    + alias2(((helper = (helper = helpers.Id || (depth0 != null ? depth0.Id : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"Id","hash":{},"data":data}) : helper)))
    + "\"\n						   href=\""
    + alias2(((helper = (helper = helpers.ProductUrl || (depth0 != null ? depth0.ProductUrl : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"ProductUrl","hash":{},"data":data}) : helper)))
    + "\">"
    + ((stack1 = ((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"Name","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>\n					</h3><span\n						class=\"product-subname\">"
    + ((stack1 = ((helper = (helper = helpers.SubName || (depth0 != null ? depth0.SubName : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"SubName","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\n				</div>\n				<div class=\"product-price\">\n"
    + ((stack1 = (helpers.ifValue || (depth0 && depth0.ifValue) || alias4).call(alias3,(depth0 != null ? depth0.DiscountedPrice : depth0),{"name":"ifValue","hash":{"equals":"-1"},"fn":container.program(16, data, 0, blockParams, depths),"inverse":container.program(21, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "				</div>\n				<!--<div class=\"product-list-article-number\"><span>"
    + alias2(((helper = (helper = helpers.Articlenumber || (depth0 != null ? depth0.Articlenumber : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"Articlenumber","hash":{},"data":data}) : helper)))
    + "</span></div>-->\n				<!-- \n				<div class=\"product-list-description\">\n					"
    + ((stack1 = (helpers.unescape || (depth0 && depth0.unescape) || alias4).call(alias3,(depth0 != null ? depth0.ShortDescription : depth0),{"name":"unescape","hash":{},"data":data})) != null ? stack1 : "")
    + "\n				</div>\n				-->\n			</div>\n		</article>\n	</div>\n</li>";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.IsAvailableInWarehouse : depth0),"==",true,{"name":"ifCond","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"3":function(container,depth0,helpers,partials,data) {
    return " product-only-available-in-warehouse";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.IsAvailableInWarehouse : depth0),"!=",true,{"name":"ifCond","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"6":function(container,depth0,helpers,partials,data) {
    return " product-only-available-in-webshop";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.ProductOverlays : depth0),{"name":"each","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "							<div class=\"pi-overlay pi-overlay-product-list "
    + alias4(((helper = (helper = helpers.OverlayPosition || (depth0 != null ? depth0.OverlayPosition : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"OverlayPosition","hash":{},"data":data}) : helper)))
    + " pi-"
    + alias4(container.lambda(((stack1 = (data && data.root)) && stack1.Language), depth0))
    + "-overlay"
    + alias4(((helper = (helper = helpers.ImageIndex || (depth0 != null ? depth0.ImageIndex : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ImageIndex","hash":{},"data":data}) : helper)))
    + " "
    + alias4(((helper = (helper = helpers.CssClass || (depth0 != null ? depth0.CssClass : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"CssClass","hash":{},"data":data}) : helper)))
    + "\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.OverlayTexts : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "							</div>\n";
},"10":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "									"
    + container.escapeExpression(container.lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.OverlayTexts : depth0)) != null ? stack1["0"] : stack1)) != null ? stack1.Value : stack1), depth0))
    + "\n";
},"12":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? depth0.Attributes : depth0)) != null ? stack1.length : stack1),"==",0,{"name":"ifCond","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"13":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.IsBuyable : depth0),{"name":"if","hash":{},"fn":container.program(14, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"14":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "					<a class=\"buy-button\" onclick=\"AddItemToCart(this,'', '"
    + alias4(((helper = (helper = helpers.Id || (depth0 != null ? depth0.Id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Id","hash":{},"data":data}) : helper)))
    + "', '"
    + alias4(((helper = (helper = helpers.currentCulture || (depth0 != null ? depth0.currentCulture : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"currentCulture","hash":{},"data":data}) : helper)))
    + "', null); \">\n						<span>"
    + alias4((helpers.translate || (depth0 && depth0.translate) || alias2).call(alias1,"buyButton",{"name":"translate","hash":{},"data":data}))
    + "</span>\n					</a>\n";
},"16":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "						<div class=\"price-standard\">\n							<span class=\"price-amount\">\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded || (depth0 && depth0.isVatIncluded) || helpers.helperMissing).call(alias1,{"name":"isVatIncluded","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(17, data, 0),"inverse":container.program(19, data, 0),"data":data})) != null ? stack1 : "")
    + "							</span>\n						</div>\n";
},"17":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "									"
    + ((stack1 = ((helper = (helper = helpers.PriceString || (depth0 != null ? depth0.PriceString : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"PriceString","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"19":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "									"
    + ((stack1 = ((helper = (helper = helpers.PriceStringWithoutVat || (depth0 != null ? depth0.PriceStringWithoutVat : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"PriceStringWithoutVat","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"21":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing;

  return "						<div class=\"price-promotion\">\n							<span class=\"price-amount\">\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded || (depth0 && depth0.isVatIncluded) || alias2).call(alias1,{"name":"isVatIncluded","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(22, data, 0),"inverse":container.program(24, data, 0),"data":data})) != null ? stack1 : "")
    + "							</span>\n						</div>			\n						<div class=\"price-previous\">\n							<span class=\"price-amount\">\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded || (depth0 && depth0.isVatIncluded) || alias2).call(alias1,{"name":"isVatIncluded","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(17, data, 0),"inverse":container.program(19, data, 0),"data":data})) != null ? stack1 : "")
    + "							</span>\n						</div>\n";
},"22":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "									"
    + ((stack1 = ((helper = (helper = helpers.DiscountedPriceString || (depth0 != null ? depth0.DiscountedPriceString : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"DiscountedPriceString","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"24":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "									"
    + ((stack1 = ((helper = (helper = helpers.DiscountedPriceStringWithoutVat || (depth0 != null ? depth0.DiscountedPriceStringWithoutVat : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"DiscountedPriceStringWithoutVat","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.ProductItems : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true,"useDepths":true});

this["J"]["views"]["dynamic-filter-gui-top/dynamic-filter-spinner"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"df-css\">\n<div class=\"df-spinner\"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>\n</div>";
},"useData":true});

this["J"]["views"]["cat-nav-mobile/list-pages"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "    <li class=\"\">\n        <a class=\"\" href=\""
    + alias4(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"url","hash":{},"data":data}) : helper)))
    + "\">\n            <span>"
    + alias4(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + "</span>\n        </a>\n    </li>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<ul id=\"list-pages\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</ul>";
},"useData":true});