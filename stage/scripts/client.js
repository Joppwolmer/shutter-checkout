"use strict";

var app = {
    name: "insert-customer-name-here",
    version: "0.1.0",
    required: {
        "J": "1.5.1"
    },
    config: {
    },
    init: function () {
        // Add custom functions to each page type,
        // Please: Do not remove the J.component.* functions

        J.pages.addToQueue("all-pages", function () {
            $('img.lazyimg').lazyload({
                threshold: 200
            });
            J.components.footer();
            J.components.jetshopLogo();
            J.components.getManufacturerList();
            J.components.pagingControll();
            J.components.removeEmptyCurrencySelector();
            J.components.removeEmptyCultureSelector();
        });

        J.pages.addToQueue("start-page", function () {
            J.components.startPageObjects();
        });

        J.pages.addToQueue("product-page", function () {
            J.components.tabSystem();
            J.components.productThumbnails();
            J.components.leftCategoryMenu();
            J.components.stockNotification();
            J.components.removeEmptyAttributeWrappers();
            J.components.removeEmptySpecificationWrapper();
        });

        J.pages.addToQueue("category-advanced-page", function () {
            J.components.categoryWrapper();
            J.components.categoryDescription();
            J.components.leftCategoryMenu();
        });

        J.pages.addToQueue("category-page", function () {
            J.components.categoryWrapper();
            J.components.categoryDescription();
            J.components.leftCategoryMenu();
        });

        J.pages.addToQueue("orderconfirmation-page", function () {
            J.components.orderConfirm();
        });

        J.pages.addToQueue("manufacturer-advanced-page", function () {
            J.components.categoryWrapper();
            J.components.categoryDescription();
            J.components.leftCategoryMenu();
        });

        J.pages.addToQueue("manufacturer-page", function () {
            J.components.categoryWrapper();
            J.components.categoryDescription();
            J.components.leftCategoryMenu();
        });

        J.pages.addToQueue("news-page", function () {

        });

        J.pages.addToQueue("checkout-page", function () {

        });

        J.pages.addToQueue("searchresult-page", function () {
            J.components.searchResultPage();
        });

        J.pages.addToQueue("my-page", function () {

        });

        J.pages.addToQueue("changepassword-page", function () {

        });

        J.pages.addToQueue("standard-page", function () {

        });

        J.pages.addToQueue("sitemap-page", function () {

        });
    },
};

J.pages.addToQueue("all-pages", app.init);