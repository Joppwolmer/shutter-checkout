//
//  J compatibility mode
//  Enables use of standalone modules
//
//  The following components are copied from v1.4.0 & need to be maintained:
//  J.api
//  J.components.removeEmptyCurrencySelector
//  J.components.removeEmptyCultureSelector
//  J.components.removeEmptyAttributeWrappers
//  J.components.removeEmptySpecificationWrapper
//  J.components.initCart
//  J.components.updateCartData
//  J.components.deleteFromCart
//  J.components.emptyCart
//  J.components.registerHandlebarHelpers
//

var removeClassRegexp = function (regex) {
    // Using 2nd signature of $.fn.removeClass:
    // $el.removeClass(removeClassRegexp(/^device-width-/));
    return function (index, classes) {
        return classes.split(/\s+/).filter(function (el) {
            return regex.test(el);
        }).join(' ');
    }
};

var Jcompat = {
    J: jQuery.extend(true, {}, J),
    version: "0.3.0",
    current: J.version,
    target: "1.4.0",
    init: function () {
        //
        // Stuff we do before J.init, added to J
        //

        // check queues for J.pages, anything new?

        if (J.version === "0.8.5") {
            console.log("Using compatibility-mode: " + this.current + " -> " + this.target);
            this.setPageType();
            this.addHandlebarHelpers();
            this.addAPI();
            this.replaceInitFoundation();
            this.updateSwitch();


            // Reset the old cart handling
            J.components.initCart = function () {
            };
            //J.components.prepareMobileCartData = function(){};
            //J.components.updateMobileCart = function(){};

            this.addMissingComponents();

            J.pages.addToQueue("all-pages", function () {
                J.components.removeEmptyCurrencySelector();
                J.components.removeEmptyCultureSelector();
            });

            J.pages.addToQueue("product-page", function () {
                J.components.removeEmptyAttributeWrappers();
                J.components.removeEmptySpecificationWrapper();
            });

            $(document).ready(function () {
                Jcompat.initDocReady();
                console.log("J.version: " + J.version);
            });


            //unbind the old switch
            Foundation.utils.helper = {
                setDeviceWidth: function () {
                }
            };

            // Activate new switch
            J.switch.check();

            // On resize
            $(window).resize(function () {
                J.switch.check();
                J.deviceWidth = J.switch.width;
            });

        } else {
            console.log("Not version 0.8.5, compatibility mode");
        }
    },
    initDocReady: function () {
        //
        // Stuff we do after J.init
        //

        Jcompat.updateVersion();
        this.addFoundationReveal();


        // Turn off old Cart binding
        //$(document).off('ajaxComplete');
        // Bind detection of cart update
        $(document).ajaxComplete(function (event, xhr, settings) {
            var data = {};
            if (settings.url === J.config.cartRESTUrl && xhr.readyState == 4 && xhr.status == 200) {
                J.components.updateCartData(xhr, false);
            }
        });

    },
    updateVersion: function () {
        J.version = this.current + " (" + this.target + " compatibility)";
    },
    setPageType: function () {
        if ($("html.page-responsive-orderconfirmed").length > 0) {
            J.checker.isOrderConfirmationPage = true;
            J.pageType = "orderconfirmation-page";
            $("body").removeClass("default-page");
            addBodyClass("orderconfirmation-page");
            executeQueue(J.pages.queues["orderconfirmation-page"]);
        } else if ($("html.page-responsive-checkout").length > 0) {
            J.checker.isCheckoutPage = true;
            J.pageType = "checkout-page";
            $("body").removeClass("default-page");
            addBodyClass("checkout-page");
            executeQueue(J.pages.queues["checkout-page"]);
        }
    },
    addHandlebarHelpers: function () {
        Handlebars.registerHelper('isStage', function () {
            if (J.data.jetshopData) return J.checker.isStage;
        });
        Handlebars.registerHelper('unescape', function (obj) {
            var doc = new DOMParser().parseFromString(arguments[0], "text/html");
            return doc.documentElement.textContent;
        });
        Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
            switch (operator) {
                case '==':
                    return (v1 == v2) ? options.fn(this) : options.inverse(this);
                case '===':
                    return (v1 === v2) ? options.fn(this) : options.inverse(this);
                case '!=':
                    return (v1 != v2) ? options.fn(this) : options.inverse(this);
                case '!==':
                    return (v1 !== v2) ? options.fn(this) : options.inverse(this);
                case '<':
                    return (v1 < v2) ? options.fn(this) : options.inverse(this);
                case '<=':
                    return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                case '>':
                    return (v1 > v2) ? options.fn(this) : options.inverse(this);
                case '>=':
                    return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                case '&&':
                    return (v1 && v2) ? options.fn(this) : options.inverse(this);
                case '||':
                    return (v1 || v2) ? options.fn(this) : options.inverse(this);
                default:
                    return options.inverse(this);
            }
        });
    },
    replaceInitFoundation: function () {
        J.components.initFoundation = function () {
            $(document).foundation(J.config.foundationConfig);
        }
    },
    updateSwitch: function () {
        J.switch.width = "";
        J.switch.queues.mediumUp = [];
        J.switch.addToMediumUp = function (queueItem) {
            this.queues.mediumUp.push(queueItem);
        };
        J.switch.check = function () {
            var queries = Foundation.media_queries;
            if (window.matchMedia(queries.xxlarge).matches) this.set("xxlarge");
            else if (window.matchMedia(queries.xlarge).matches) this.set("xlarge");
            else if (window.matchMedia(queries.large).matches) this.set("large");
            else if (window.matchMedia(queries.medium).matches) this.set("medium");
            else if (window.matchMedia(queries.small).matches) this.set("small");
        };
        J.switch.set = function (width) {
            if (width != this.width) {
                var oldWidth = this.width;
                if (oldWidth === "small") executeQueue(this.queues.mediumUp);
                if (width === "small") executeQueue(this.queues.small);
                else if (width === "medium") executeQueue(this.queues.medium);
                else if (Foundation.utils.is_large_up()) {
                    if (oldWidth === "large" || oldWidth === "xlarge" || oldWidth === "xxlarge") {
                        var noop;
                    } else {
                        executeQueue(this.queues.largeUp);
                    }
                }
                $(window).trigger('switch', [width, oldWidth]);
                $("body").removeClass(removeClassRegexp(/^device-width-/)).addClass("device-width-" + width);
                this.width = width;
            }
        };
    },
    addMissingComponents: function () {
        if (typeof(J.components.removeEmptyCurrencySelector) === "undefined") {
            J.components.removeEmptyCurrencySelector = function () {
                var currencySelector = $("#footer").find(".currency-selector-wrapper");
                if (currencySelector.length) {
                    if ($(".currency-selector-item").length) {
                        currencySelector.addClass("currency-selector-radios");
                    } else {
                        currencySelector.addClass("currency-selector-dropdown");
                        var currencySelectorOptions = currencySelector.find("option");
                        if (currencySelectorOptions.length < 2) {
                            currencySelector.remove()
                        }
                    }
                }
            };
        }

        if (typeof(J.components.removeEmptyCultureSelector) === "undefined") {
            J.components.removeEmptyCultureSelector = function () {
                var cultureSelector = $("#footer").find(".culture-selector-wrapper");
                if (cultureSelector.length) {
                    if (cultureSelector.find(".culture-selector-clickable").length) {
                        // Type: Flags
                        if (cultureSelector.find("input").length < 2) {
                            cultureSelector.remove();
                        }
                    } else if (cultureSelector.find("option").length) {
                        // Type: DropDown
                        cultureSelector.addClass("culture-selector-dropdown");
                        if (cultureSelector.find("option").length < 2) {
                            cultureSelector.remove();
                        }
                    } else {
                        // Type: Text
                        if (cultureSelector.find("a").length < 2) {
                            cultureSelector.remove();
                        }
                    }
                }
            };
        }

        if (typeof(J.components.removeEmptyAttributeWrappers) === "undefined") {
            J.components.removeEmptyAttributeWrappers = function () {
                $(".product-attributes > div").each(function () {
                    if (!$(this).find("select").length) {
                        $(this).remove();
                    }
                });
            };
        }

        if (typeof(J.components.removeEmptySpecificationWrapper) === "undefined") {
            J.components.removeEmptySpecificationWrapper = function () {
                var specificationWrapper = $(".product-specification-wrapper");
                if (!specificationWrapper.find("select").length) {
                    specificationWrapper.remove();
                }
            };
        }

        J.components.initCart = function () {
            J.cart = {
                cartId: getCookie("JetShop_CartID")
            };
        };

        if (typeof(J.components.updateCartData) === "undefined") {
            J.components.updateCartData = function (xhr, cartApiData) {
                if (xhr) {
                    // ADD DATA ONLY REACHABLE FROM REGULAR CART UPDATE (FREE SHIPPING TEXT)
                    var xmlDOM = (new DOMParser()).parseFromString(xhr.responseText, 'text/xml');
                    var temp = $(xmlDOM.getElementsByTagName("DivContents")[0].childNodes[1]).text();
                    var tempObj = $(temp).text();
                    if (tempObj.length > 0) {
                        J.cart.freeFreightMessage = $(temp).text();
                    } else {
                        J.cart.freeFreightMessage = false;
                    }
                }
                if (cartApiData) {
                    // DATA ALREADY EXISTS - UPDATE CART OBJECT
                    $.each(cartApiData, function (propName, propValue) {
                        J.cart[propName] = propValue;
                    });
                    // Trigger event for modules
                    $(window).triggerHandler('cart-updated');
                } else {
                    // NO EXISTING DATA - GET FROM API
                    var restUrl = "/services/rest/v2/json/" + JetshopData.Culture + "/" + JetshopData.Currency + "/shoppingcart/" + J.cart.cartId;
                    if (J.checker.isLoggedIn) {
                        restUrl += "?pricelistid=" + JetshopData.PriceListId;
                    }
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: restUrl,
                        success: function (data) {
                            var json = typeof data == 'object' ? data : $.parseJSON('(' + data + ')');
                            cartApiData = json[0];
                            // SEND RECEIVED DATA BACK INTO DATA UPDATE FUNCTION
                            J.components.updateCartData(false, cartApiData);
                        },
                        error: function (request, status, error) {
                            console.error("Cart data fetch error:");
                            console.log(error);
                        }
                    });
                    $(window).triggerHandler('cart-loaded');
                }
            };
        }

        if (typeof(J.components.deleteFromCart) === "undefined") {
            J.components.deleteFromCart = function (recId) {
                var restUrl = "/services/rest/v2/json/" + J.data.culture + "/" + J.data.currency.name + "/shoppingcart/delete/" + J.cart.cartId;
                if (JetshopData.IsLoggedIn) {
                    restUrl += "?pricelistid=" + J.data.priceListId;
                }
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: restUrl,
                    data: '{ "RecId": ' + recId + '}',
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        var json = typeof data == 'object' ? data : $.parseJSON('(' + data + ')');
                        var cartApiData = json[0];
                        J.components.updateCartData(false, cartApiData);
                        Services.cartService.reload();
                        if (J.checker.isCheckoutPage) {
                            location.reload();
                        }
                    },
                    error: function (request, status, error) {
                        console.error('Ajax Error in J.components.deleteFromCart:');
                        console.log(request);
                        console.log(status);
                        console.log(error);
                    }
                });
            };
        }

        if (typeof(J.components.emptyCart) === "undefined") {
            J.components.emptyCart = function () {
                if (J.cart.ProductsCart.length > 0) {
                    for (var item in J.cart.ProductsCart) {
                        J.components.deleteFromCart(J.cart.ProductsCart[item]["RecId"]);
                    }
                }
            }
        }

    },
    addAPI: function () {
        J.api = {
            helpers: {
                baseUrl: function (includeCulture, includeCurrency) {
                    var url = window.location.protocol + "//" + window.location.hostname + "/Services/Rest/v2/json/";
                    if (includeCulture) url += J.data.culture + "/";
                    if (includeCurrency) url += J.data.currency.name + "/";
                    return url;
                },
                //TODO when adding cart-services we need to add method postAjax
                getAjax: function (service, callback, callbackOptions, enableCache, cacheMinutes, includeCulture, includeCurrency) {
                    //console.info("Actually fetching AJAX: " + service);
                    $.ajax({
                        type: "GET",
                        url: J.api.helpers.baseUrl(includeCulture, includeCurrency) + service,
                        success: function (data, textStatus, jqXHR) {
                            if (enableCache) {
                                J.api.helpers.save(J.api.helpers.baseUrl(includeCulture, includeCurrency) + service, cacheMinutes, data);
                            }
                            if (typeof callback == "function") {
                                callback(data, callbackOptions);
                            } else if (typeof callback.success == "function") {
                                callback.success(data, callbackOptions, textStatus, jqXHR);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            var error = {"error": "Ajax fail", "service": service};
                            J.api.helpers.raiseError(error);
                            if (typeof callback.error == "function") {
                                callback.error(callbackOptions, xhr, ajaxOptions, thrownError);
                            }
                        },
                        complete: function (event, xhr) {
                            if (typeof callback.complete == "function") {
                                var data = JSON.parse(event.responseText);
                                callback.complete(data, callbackOptions, event, xhr);
                            }
                        }
                    });
                },
                getter: function (service, callback, callbackOptions, enableCache, cacheMinutes, includeCulture, includeCurrency) {
                    if (typeof callback === "undefined") {
                        var error = {"error": "No callback", "service": service};
                        J.api.helpers.raiseError(error);
                    } else {
                        if (typeof callback.before == "function") {
                            callback.before(callbackOptions);
                        }
                        if (enableCache) {
                            var itemId = J.api.helpers.baseUrl(includeCulture, includeCurrency) + service;
                            if (J.api.helpers.read(itemId, cacheMinutes)) {
                                var cachedItem = J.api.helpers.read(itemId, cacheMinutes);
                                var minutes = Math.floor(Math.abs(cachedItem.timeStamp - Date.now()) / 60000);
                                if (minutes < cacheMinutes) {
                                    var jqXHR = cachedItem.data;
                                    var textStatus = "success";
                                    if (typeof callback == "function") {
                                        callback(cachedItem.data, callbackOptions);
                                    } else if (typeof callback.success == "function") {
                                        callback.success(cachedItem.data, callbackOptions, textStatus, jqXHR);
                                    }
                                    if (typeof callback.complete == "function") {
                                        callback.complete(cachedItem.data, callbackOptions, textStatus);
                                    }
                                } else {
                                    J.api.helpers.getAjax(service, callback, callbackOptions, enableCache, cacheMinutes, includeCulture, includeCurrency);
                                }
                            } else {
                                J.api.helpers.getAjax(service, callback, callbackOptions, enableCache, cacheMinutes, includeCulture, includeCurrency);
                            }
                        } else {
                            J.api.helpers.getAjax(service, callback, callbackOptions, enableCache, cacheMinutes, includeCulture, includeCurrency);
                        }
                    }
                },
                read: function (id, cacheMinutes) {
                    var item = localStorage.getItem(J.config.apiCachePrefix + id);
                    if (item === "") {
                        return false;
                    } else {
                        return JSON.parse(item);
                    }
                },
                save: function (id, cacheMinutes, data) {
                    //log("Caching item: " + id);
                    var storage = isLocalStorageNameSupported();
                    if (storage) {
                        var timeStamp = Date.now();
                        var item = {"cacheMinutes": cacheMinutes, "timeStamp": timeStamp, "data": data};
                        try {
                            localStorage.setItem(J.config.apiCachePrefix + id, JSON.stringify(item));
                        } catch (e) {
                            if (J.api.helpers.isQuotaExceeded(e)) {
                                console.log("Error storing local storage");
                            }
                        }
                    }
                },
                clearCache: function () {
                    for (var key in window.localStorage) {
                        if (key.indexOf(J.config.apiCachePrefix) != -1) {
                            localStorage.removeItem(key);
                        }
                    }
                    console.log("Cleared cache.");
                },
                initCache: function () {
                    if ($(".vat-selector-input input").length > 0) {
                        var vatInputElementId = $(".vat-selector-input input").attr("id");
                        var vatInputElement = document.getElementById(vatInputElementId);
                        vatInputElement.onclick = function () {
                            J.api.helpers.clearCache();
                            setTimeout('__doPostBack(\'' + vatInputElementId + '\',\'\')', 0);
                        }
                    }
                },
                raiseError: function (error) {
                    console.error(error);
                },
                isQuotaExceeded: function (error) {
                    var quotaExceeded = false;
                    if (error) {
                        if (error.code) {
                            switch (error.code) {
                                case 22:
                                    quotaExceeded = true;
                                    break;
                                case 1014:
                                    // Firefox
                                    if (error.name === 'NS_ERROR_DOM_QUOTA_REACHED') {
                                        quotaExceeded = true;
                                    }
                                    break;
                            }
                        } else if (error.number === -2147024882) {
                            // Internet Explorer 8
                            quotaExceeded = true;
                        }
                    }
                    return quotaExceeded;
                },
                createObject: function (data) {
                    return {
                        TotalProducts: data.length,
                        TotalPages: 1,
                        ProductsInPage: data.length,
                        ProductItems: data,
                        PageSize: 0
                    };
                }
            },
            product: {
                get: function (callback, callbackOptions, enableCache, cacheMinutes, id) {
                    if (typeof id === "number") {
                        var service = "products/" + id;
                        J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes);
                    } else {
                        var list = id;
                        var products = [];
                        var add = function (data) {
                            products.push(data.ProductItems[0]);
                            if (list.length == products.length) {
                                var obj = J.api.helpers.createObject(products);
                                callback(obj, callbackOptions);
                            }
                        };
                        for (var i = 0; i < list.length; i++) {
                            J.api.product.get(add, i, enableCache, cacheMinutes, list[i]);
                        }
                    }
                },
                getRelatedProducts: function (callback, callbackOptions, enableCache, cacheMinutes, id) {
                    var products = [];
                    var total = 0;
                    var get = function (productData, id) {
                        var list = productData.ProductItems[0].RelatedProducts;
                        total = list.length;
                        for (var i = 0; i < list.length; i++) {
                            J.api.product.get(add, null, enableCache, cacheMinutes, list[i]);
                        }
                    };
                    var add = function (data) {
                        products.push(data.ProductItems[0]);
                        if (total == products.length) {
                            callback(products, callbackOptions);
                        }
                    };
                    J.api.product.get(get, id, enableCache, cacheMinutes, id);
                }
            },
            category: {
                get: function (callback, callbackOptions, enableCache, cacheMinutes, id, displayTypeWeb) {
                    var service = "categories/" + id;
                    if (displayTypeWeb) service += "?displayType=web";
                    J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes);
                },
                getProducts: function (callback, callbackOptions, enableCache, cacheMinutes, id, full, rows, page) {
                    var service = "categories/" + id + "/products";
                    if (full) service += "/full";
                    if (rows) service += "/" + rows;
                    if (page) service += "/" + page;
                    J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, true);
                },
                list: function (callback, callbackOptions, enableCache, cacheMinutes) {
                    var service = "categories";
                    J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, false);
                }
            },
            page: {
                get: function (callback, callbackOptions, enableCache, cacheMinutes, id) {
                    var service = "pages/" + id;
                    J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, false);
                },
                list: function (callback, callbackOptions, enableCache, cacheMinutes) {
                    var service = "pages";
                    J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, false);
                }
            },
            search: function (callback, callbackOptions, enableCache, cacheMinutes, string, full, rows, page) {
                var service = "search/";
                if (full) {
                    service += "full/" + string;
                } else {
                    service += string;
                }
                if (rows) service += "/" + rows;
                if (page) service += "/" + page;
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, false);
            },
            settings: {
                get: function (callback, callbackOptions, enableCache, cacheMinutes) {
                    var service = "settings";
                    J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, false, false);
                }
            },
            startpage: {
                get: function (callback, callbackOptions, enableCache, cacheMinutes, id) {
                    var service = "customstartpage/" + id;
                    J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, false);
                },
                list: function (callback, callbackOptions, enableCache, cacheMinutes) {
                    var service = "customstartpages";
                    J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, false, false);
                },
                active: function (callback, callbackOptions, enableCache, cacheMinutes) {
                    var service = "activestartpage";
                    J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, false, false);
                }
            }
        };
        J.api.initCache = J.api.helpers.initCache;
    },
    addFoundationReveal: function () {
        console.log("Foundation version: " + Foundation.version);
        if ((Foundation.version == "5.5.2") && (typeof(Foundation.libs.reveal) === "undefined")) {
            !function(a,b,c,d){"use strict";function e(a){var b=/fade/i.test(a),c=/pop/i.test(a);return{animate:b||c,pop:c,fade:b}}Foundation.libs.reveal={name:"reveal",version:"5.5.2",locked:!1,settings:{animation:"fadeAndPop",animation_speed:250,close_on_background_click:!0,close_on_esc:!0,dismiss_modal_class:"close-reveal-modal",multiple_opened:!1,bg_class:"reveal-modal-bg",root_element:"body",open:function(){},opened:function(){},close:function(){},closed:function(){},on_ajax_error:a.noop,bg:a(".reveal-modal-bg"),css:{open:{opacity:0,visibility:"visible",display:"block"},close:{opacity:1,visibility:"hidden",display:"none"}}},init:function(b,c,d){a.extend(!0,this.settings,c,d),this.bindings(c,d)},events:function(a){var b=this,d=b.S;return d(this.scope).off(".reveal").on("click.fndtn.reveal","["+this.add_namespace("data-reveal-id")+"]:not([disabled])",function(a){if(a.preventDefault(),!b.locked){var c=d(this),e=c.data(b.data_attr("reveal-ajax")),f=c.data(b.data_attr("reveal-replace-content"));if(b.locked=!0,"undefined"==typeof e)b.open.call(b,c);else{var g=e===!0?c.attr("href"):e;b.open.call(b,c,{url:g},{replaceContentSel:f})}}}),d(c).on("click.fndtn.reveal",this.close_targets(),function(a){if(a.preventDefault(),!b.locked){var c=d("["+b.attr_name()+"].open").data(b.attr_name(!0)+"-init")||b.settings,e=d(a.target)[0]===d("."+c.bg_class)[0];if(e){if(!c.close_on_background_click)return;a.stopPropagation()}b.locked=!0,b.close.call(b,e?d("["+b.attr_name()+"].open:not(.toback)"):d(this).closest("["+b.attr_name()+"]"))}}),d("["+b.attr_name()+"]",this.scope).length>0?d(this.scope).on("open.fndtn.reveal",this.settings.open).on("opened.fndtn.reveal",this.settings.opened).on("opened.fndtn.reveal",this.open_video).on("close.fndtn.reveal",this.settings.close).on("closed.fndtn.reveal",this.settings.closed).on("closed.fndtn.reveal",this.close_video):d(this.scope).on("open.fndtn.reveal","["+b.attr_name()+"]",this.settings.open).on("opened.fndtn.reveal","["+b.attr_name()+"]",this.settings.opened).on("opened.fndtn.reveal","["+b.attr_name()+"]",this.open_video).on("close.fndtn.reveal","["+b.attr_name()+"]",this.settings.close).on("closed.fndtn.reveal","["+b.attr_name()+"]",this.settings.closed).on("closed.fndtn.reveal","["+b.attr_name()+"]",this.close_video),!0},key_up_on:function(a){var b=this;return b.S("body").off("keyup.fndtn.reveal").on("keyup.fndtn.reveal",function(a){var c=b.S("["+b.attr_name()+"].open"),d=c.data(b.attr_name(!0)+"-init")||b.settings;d&&27===a.which&&d.close_on_esc&&!b.locked&&b.close.call(b,c)}),!0},key_up_off:function(a){return this.S("body").off("keyup.fndtn.reveal"),!0},open:function(c,d){var f,e=this;c?"undefined"!=typeof c.selector?f=e.S("#"+c.data(e.data_attr("reveal-id"))).first():(f=e.S(this.scope),d=c):f=e.S(this.scope);var g=f.data(e.attr_name(!0)+"-init");if(g=g||this.settings,f.hasClass("open")&&c.attr("data-reveal-id")==f.attr("id"))return e.close(f);if(!f.hasClass("open")){var h=e.S("["+e.attr_name()+"].open");if("undefined"==typeof f.data("css-top")&&f.data("css-top",parseInt(f.css("top"),10)).data("offset",this.cache_offset(f)),f.attr("tabindex","0").attr("aria-hidden","false"),this.key_up_on(f),f.on("open.fndtn.reveal",function(a){"fndtn.reveal"!==a.namespace}),f.on("open.fndtn.reveal").trigger("open.fndtn.reveal"),h.length<1&&this.toggle_bg(f,!0),"string"==typeof d&&(d={url:d}),"undefined"!=typeof d&&d.url){var i="undefined"!=typeof d.success?d.success:null;a.extend(d,{success:function(b,c,d){if(a.isFunction(i)){var j=i(b,c,d);"string"==typeof j&&(b=j)}"undefined"!=typeof options&&"undefined"!=typeof options.replaceContentSel?f.find(options.replaceContentSel).html(b):f.html(b),e.S(f).foundation("section","reflow"),e.S(f).children().foundation(),h.length>0&&(g.multiple_opened?e.to_back(h):e.hide(h,g.css.close)),e.show(f,g.css.open)}}),g.on_ajax_error!==a.noop&&a.extend(d,{error:g.on_ajax_error}),a.ajax(d)}else h.length>0&&(g.multiple_opened?e.to_back(h):e.hide(h,g.css.close)),this.show(f,g.css.open)}e.S(b).trigger("resize")},close:function(b){var b=b&&b.length?b:this.S(this.scope),c=this.S("["+this.attr_name()+"].open"),d=b.data(this.attr_name(!0)+"-init")||this.settings,e=this;c.length>0&&(b.removeAttr("tabindex","0").attr("aria-hidden","true"),this.locked=!0,this.key_up_off(b),b.trigger("close.fndtn.reveal"),(d.multiple_opened&&1===c.length||!d.multiple_opened||b.length>1)&&(e.toggle_bg(b,!1),e.to_front(b)),d.multiple_opened?(e.hide(b,d.css.close,d),e.to_front(a(a.makeArray(c).reverse()[1]))):e.hide(c,d.css.close,d))},close_targets:function(){var a="."+this.settings.dismiss_modal_class;return this.settings.close_on_background_click?a+", ."+this.settings.bg_class:a},toggle_bg:function(b,c){0===this.S("."+this.settings.bg_class).length&&(this.settings.bg=a("<div />",{class:this.settings.bg_class}).appendTo("body").hide());var e=this.settings.bg.filter(":visible").length>0;c!=e&&((c==d?e:!c)?this.hide(this.settings.bg):this.show(this.settings.bg))},show:function(c,d){if(d){var f=c.data(this.attr_name(!0)+"-init")||this.settings,g=f.root_element,h=this;if(0===c.parent(g).length){var i=c.wrap('<div style="display: none;" />').parent();c.on("closed.fndtn.reveal.wrapped",function(){c.detach().appendTo(i),c.unwrap().unbind("closed.fndtn.reveal.wrapped")}),c.detach().appendTo(g)}var j=e(f.animation);if(j.animate||(this.locked=!1),j.pop){d.top=a(b).scrollTop()-c.data("offset")+"px";var k={top:a(b).scrollTop()+c.data("css-top")+"px",opacity:1};return setTimeout(function(){return c.css(d).animate(k,f.animation_speed,"linear",function(){h.locked=!1,c.trigger("opened.fndtn.reveal")}).addClass("open")},f.animation_speed/2)}if(j.fade){d.top=a(b).scrollTop()+c.data("css-top")+"px";var k={opacity:1};return setTimeout(function(){return c.css(d).animate(k,f.animation_speed,"linear",function(){h.locked=!1,c.trigger("opened.fndtn.reveal")}).addClass("open")},f.animation_speed/2)}return c.css(d).show().css({opacity:1}).addClass("open").trigger("opened.fndtn.reveal")}var f=this.settings;return e(f.animation).fade?c.fadeIn(f.animation_speed/2):(this.locked=!1,c.show())},to_back:function(a){a.addClass("toback")},to_front:function(a){a.removeClass("toback")},hide:function(c,d){if(d){var f=c.data(this.attr_name(!0)+"-init"),g=this;f=f||this.settings;var h=e(f.animation);if(h.animate||(this.locked=!1),h.pop){var i={top:-a(b).scrollTop()-c.data("offset")+"px",opacity:0};return setTimeout(function(){return c.animate(i,f.animation_speed,"linear",function(){g.locked=!1,c.css(d).trigger("closed.fndtn.reveal")}).removeClass("open")},f.animation_speed/2)}if(h.fade){var i={opacity:0};return setTimeout(function(){return c.animate(i,f.animation_speed,"linear",function(){g.locked=!1,c.css(d).trigger("closed.fndtn.reveal")}).removeClass("open")},f.animation_speed/2)}return c.hide().css(d).removeClass("open").trigger("closed.fndtn.reveal")}var f=this.settings;return e(f.animation).fade?c.fadeOut(f.animation_speed/2):c.hide()},close_video:function(b){var c=a(".flex-video",b.target),d=a("iframe",c);d.length>0&&(d.attr("data-src",d[0].src),d.attr("src",d.attr("src")),c.hide())},open_video:function(b){var c=a(".flex-video",b.target),e=c.find("iframe");if(e.length>0){var f=e.attr("data-src");if("string"==typeof f)e[0].src=e.attr("data-src");else{var g=e[0].src;e[0].src=d,e[0].src=g}c.show()}},data_attr:function(a){return this.namespace.length>0?this.namespace+"-"+a:a},cache_offset:function(a){var b=a.show().height()+parseInt(a.css("top"),10)+a.scrollY;return a.hide(),b},off:function(){a(this.scope).off(".fndtn.reveal")},reflow:function(){}}}(jQuery,window,window.document);
            // Inject CSS
            var cssString = '.reveal-modal-bg{background:#000;background:rgba(0,0,0,.45);bottom:0;display:none;position:fixed;right:0;top:0;z-index:1004;left:0}.reveal-modal{border-radius:3px;display:none;position:absolute;top:0;visibility:hidden;width:100%;z-index:1005;left:0;background-color:#FFF;padding:1.875rem;border:1px solid #666;box-shadow:0 0 10px rgba(0,0,0,.4)}@media only screen and (max-width:40em){.reveal-modal{min-height:100vh}}.reveal-modal .column,.reveal-modal .columns{min-width:0}.reveal-modal>:first-child{margin-top:0}.reveal-modal>:last-child{margin-bottom:0}.reveal-modal.radius{border-radius:3px}.reveal-modal.round{border-radius:1000px}.reveal-modal.collapse{padding:0}@media only screen and (min-width:40.0625em){.reveal-modal{left:0;margin:0 auto;max-width:62.5rem;right:0;width:80%;top:6.25rem}.reveal-modal.tiny{left:0;margin:0 auto;max-width:62.5rem;right:0;width:30%}.reveal-modal.small{left:0;margin:0 auto;max-width:62.5rem;right:0;width:40%}.reveal-modal.medium{left:0;margin:0 auto;max-width:62.5rem;right:0;width:60%}.reveal-modal.large{left:0;margin:0 auto;max-width:62.5rem;right:0;width:70%}.reveal-modal.xlarge{left:0;margin:0 auto;max-width:62.5rem;right:0;width:95%}}.reveal-modal.full{height:100vh;height:100%;left:0;margin-left:0!important;max-width:none!important;min-height:100vh;top:0}@media only screen and (min-width:40.0625em){.reveal-modal.full{left:0;margin:0 auto;max-width:62.5rem;right:0;width:100%}}.reveal-modal.toback{z-index:1003}.reveal-modal .close-reveal-modal{color:#AAA;cursor:pointer;font-size:2.5rem;font-weight:700;line-height:1;position:absolute;top:.625rem;right:1.375rem}';
            var node = document.createElement('style');
            node.innerHTML = cssString;
            document.body.appendChild(node);
        }
    }
};

Jcompat.init();
